FROM python:3.11-alpine

COPY . /opt/parking
WORKDIR /opt/parking

RUN pip install -r requirements.txt