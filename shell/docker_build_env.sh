#! /bin/bash

REPO_URL=$(git config --get remote.origin.url)
REPO_NAME=$(basename "${REPO_URL}" ".git")
SSH_USER=$(tr -s '=' <config | awk 'NR==3' | cut -d'=' -f2)
SSH_HOST=$(tr -s '=' <config | awk 'NR==1' | cut -d'=' -f2)
SSH_PORT=$(tr -s '=' <config | awk 'NR==2' | cut -d'=' -f2)
BUILD_IMAGE_NAME=${REPO_NAME}:latest
DOCKER_DEFAULT_PLATFORM=linux/amd64

echo "${BUILD_IMAGE_NAME}" > .prefect_build_image

cat << EOF
DOCKER_DEFAULT_PLATFORM=$DOCKER_DEFAULT_PLATFORM
BUILD_IMAGE_NAME=$BUILD_IMAGE_NAME
SSH_USER=$SSH_USER
SSH_HOST=$SSH_HOST
SSH_PORT=$SSH_PORT
EOF
