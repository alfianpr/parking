# Parking Management System

This repository contains a parking management system built with a FastAPI backend and a Bootstrap-based frontend. The system allows users to manage car parking, including adding and removing cars, and calculating parking bills.

## Setup

### Prerequisites

- Python 3.8+
- PostgreSQL
- Node.js and npm (for managing frontend dependencies)

### Installation

1. **Clone the repository**:

    ```bash
    git clone https://gitlab.com/alfianpr/parking.git
    cd parking
    ```

2. **Set up the backend**:

    ```bash
    python -m venv env
    source env/bin/activate  # On Windows use `env\Scripts\activate`
    pip install -r requirements.txt
    ```

3. **Configure PostgreSQL**:

    - Ensure PostgreSQL is installed and running.
    - Create a database named `parking`:

    ```sql
    CREATE DATABASE parking;
    ```

4. **Run the backend**:

    ```bash
    uvicorn parking_api:app --reload
    ```

5. **Set up the frontend**:

    - The frontend dependencies are included via CDN, so no additional setup is required.

6. **Open the page**:

    - Open using firefox
    
    ```
    firefox index.html
    ```
