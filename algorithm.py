def get_max_min(arr, k):
    """
    Find the maximum of the minimums of all subarrays of size k in the given array.

    Parameters:
        arr (List[int]): The input array.
        k (int): The size of the subarrays.

    Returns:
        int: The maximum of the minimums of all subarrays.
    """
    min_values = []

    for i in range(len(arr) - k + 1):
        subset = arr[i:i + k]
        min_value = min(subset)
        min_values.append(min_value)
    max_of_minimums = max(min_values)
    
    return max_of_minimums

if __name__ == '__main__':
    # Test the function
    arr = [1, 2, 3, 4, 5]
    k = 3
    print(
        get_max_min(arr, k)
    )