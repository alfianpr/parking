let cars = [];
const addCarButton = document.querySelector('#carButton');
const minLicenseeLength = 4;
const payPerHour = 3000;
const payFirstHour = 5000;
const totalPlaces = 10;
const timeOutFreeSpot = 5000; // in milliseconds

const API_BASE_URL = "http://93.127.206.143:8000";

const formatDate = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? '0' + minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + strTime;
}

const secondsToHours = (d) => {
  d = Number(d);
  let h = Math.ceil(d / 3600);
  return h;
}

const renderTable = () => {
  let results = '';
  for (var i = cars.length - 1; i >= 0; i--) {
    let licensee = cars[i].licensee;
    let arrival = formatDate(new Date(cars[i].arrival));
    let leave = cars[i].leave === null ? '-' : formatDate(new Date(cars[i].leave));

    results += `<tr>
      <td>${licensee}</td>
      <td>${arrival}</td>
      <td>${leave}</td>
      <td>${showStatus(cars[i])}</td>
      <td class="text-right">${makeBill(cars[i])}</td>
      <td class="text-right">
        <button data-row="${i}" onclick="showSummary(event)" data-toggle="modal" data-target="#myModal" class="btn btn-sm btn-success">Summary</button>
      </td>
    </tr>`;
  }

  document.querySelector("#parking tbody").innerHTML = results;
}

const showStatus = (car) => {
  return car.is_parked ? "Parked" : "Has left";
}

const changeStatus = async (licensee) => {
  try {
    const response = await fetch(`${API_BASE_URL}/parked-cars/${licensee}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const updatedCar = await response.json();
    return updatedCar;
  } catch (error) {
    console.error("Error changing status:", error);
  }
}

const countAvailablePlaces = () => {
  document.querySelector('#placesCount').innerHTML = totalPlaces - cars.filter(car => car.is_parked).length;
}

const setClassForBadge = () => {
  let badgeClassName = cars.filter(car => car.is_parked).length == totalPlaces ? 'badge badge-danger' : 'badge badge-success';
  document.querySelector('#placesCount').setAttribute('class', badgeClassName);
}

const calculateHoursBilled = (car) => {
  let arrivedAt = new Date(car.arrival).getTime();
  let leftAt = new Date(car.leave).getTime();
  return secondsToHours((leftAt - arrivedAt) / 1000); // duration in seconds
}

const makeBill = (car) => {
  if (car.leave === null) return "-";
  let hoursBilled = calculateHoursBilled(car);
  let billValue = "Rp " + (payFirstHour + (hoursBilled - 1) * payPerHour);
  return billValue;
}

const printSummary = (event) => {
  let car = cars[event.target.dataset.row];
  let sumarryTable = `<table class="table table-bordered m-0">
    <tr>
      <td class="font-weight-bold">Registration number</td>
      <td>${car.licensee}</td>
    </tr>
    <tr>
      <td class="font-weight-bold">Arrival</td>
      <td>${formatDate(new Date(car.arrival))}</td>
    </tr>
    <tr>
      <td class="font-weight-bold">Departure</td>
      <td>${car.leave === null ? '-' : formatDate(new Date(car.leave))}</td>
    </tr>
    <tr>
      <td class="font-weight-bold">Billable hours</td>
      <td>${calculateHoursBilled(car)}</td>
    </tr>
    <tr>
      <td class="font-weight-bold">Bill value</td>
      <td>${makeBill(car)}</td>
    </tr></table>`;

  document.querySelector('#modalBody').innerHTML = sumarryTable;
}

const showSummary = async (event) => {
  let car = cars[event.target.dataset.row];
  const updatedCar = await changeStatus(car.licensee);
  if (updatedCar) {
    cars[event.target.dataset.row] = updatedCar;
    renderTable();
    printSummary(event);
    setTimeout(() => {
      freeSpot(event);
    }, timeOutFreeSpot);
  }
}

const addCar = async () => {
  let newLicensee = document.querySelector("#carValue").value;

  if (newLicensee.length >= minLicenseeLength && cars.filter(car => car.is_parked).length < totalPlaces) {
    try {
      const response = await fetch(`${API_BASE_URL}/parked-cars`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ licensee: newLicensee })
      });

      if (response.ok) {
        const newCar = await response.json();
        cars.push(newCar);
        setClassForBadge();
        countAvailablePlaces();
        document.querySelector("#carValue").value = '';
        renderTable();
      } else {
        const error = await response.json();
        document.querySelector('#message').innerText = error.detail;
        document.querySelector('#message').style.display = 'block';
      }
    } catch (error) {
      console.error("Error adding car:", error);
    }
  } else {
    document.querySelector('#message').innerText = newLicensee.length < minLicenseeLength ? "License plate number is too short" : "Parking is full";
    document.querySelector('#message').style.display = 'block';
  }

  if (cars.filter(car => car.is_parked).length == totalPlaces) {
    document.querySelector('#carButton').setAttribute('disabled', true);
  }
}

const freeSpot = (event) => {
  cars.splice(event.target.dataset.row, 1);
  setClassForBadge();
  document.querySelector('#carButton').removeAttribute('disabled');
  renderTable();
  countAvailablePlaces();
}

const fetchParkedCars = async () => {
  try {
    const response = await fetch(`${API_BASE_URL}/parked-cars`);
    cars = await response.json();
    renderTable();
    countAvailablePlaces();
    setClassForBadge();
  } catch (error) {
    console.error("Error fetching parked cars:", error);
  }
}

// Add new car to the array
if (addCarButton) {
  addCarButton.addEventListener('click', addCar);
}

// Fetch parked cars on page load
fetchParkedCars();