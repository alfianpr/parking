from fastapi import (
    FastAPI, 
    HTTPException, 
    Depends
)
from pydantic import BaseModel
from datetime import datetime
from typing import List, Optional
from sqlalchemy import (
    create_engine, 
    Column, 
    Integer, 
    String, 
    DateTime, 
    Boolean
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    sessionmaker, 
    Session
)
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy_utils import (
    database_exists, 
    create_database
)
import os

# Database configuration
DATABASE_URL = f"postgresql://{os.environ['POSTGRES_USER']}:{os.environ['POSTGRES_PASSWORD']}@{os.environ['SSH_HOST']}:5432/parking"

# Create database engine
engine = create_engine(DATABASE_URL)
if not database_exists(engine.url):
    create_database(engine.url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

# Define the Car model
class Car(Base):
    __tablename__ = "cars"
    id = Column(Integer, primary_key=True, index=True)
    licensee = Column(String, index=True)
    arrival = Column(DateTime, default=datetime.utcnow)
    leave = Column(DateTime, nullable=True)
    is_parked = Column(Boolean, default=True)

# Create tables
Base.metadata.create_all(bind=engine)

# FastAPI app instance
app = FastAPI()

# Configure CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allow all origins
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],  # Allow methods
    allow_headers=["*"],  # Allow headers
)

# Pydantic models
class CarBase(BaseModel):
    licensee: str

class ParkedCar(CarBase):
    arrival: datetime
    leave: Optional[datetime]
    is_parked: bool

class ParkedCarCreate(CarBase):
    pass

# Configuration variables
min_licensee_length = 4
pay_per_hour = 3000
pay_first_hour = 5000
total_places = 10

# Dependency to get DB session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Function to get list of parked cars
def fetch_parked_cars(db: Session):
    return db.query(Car).filter(Car.is_parked == True).all()

# Function to add a car to the parking lot
def create_parked_car(car: ParkedCarCreate, db: Session):
    if len(car.licensee) < min_licensee_length:
        raise HTTPException(status_code=400, detail="License plate number is too short")
    if db.query(Car).filter(Car.is_parked == True).count() >= total_places:
        raise HTTPException(status_code=400, detail="Parking is full")
    new_car = Car(licensee=car.licensee, arrival=datetime.now(), is_parked=True)
    db.add(new_car)
    db.commit()
    db.refresh(new_car)
    return new_car

# Function to remove a car from the parking lot and calculate the bill
def update_car_leave(licensee: str, db: Session):
    car = db.query(Car).filter(Car.licensee == licensee, Car.is_parked == True).first()
    if car is None:
        raise HTTPException(status_code=404, detail="Car not found or already left")
    car.leave = datetime.now()
    car.is_parked = False
    db.commit()
    hours_parked = (car.leave - car.arrival).seconds // 3600
    bill = pay_first_hour + max(0, hours_parked - 1) * pay_per_hour
    return {
        "licensee": car.licensee,
        "arrival": car.arrival,
        "leave": car.leave,
        "is_parked": car.is_parked,
        "bill": bill
    }

# FastAPI endpoints
@app.get("/parked-cars", response_model=List[ParkedCar])
async def get_parked_cars(db: Session = Depends(get_db)):
    return fetch_parked_cars(db)

@app.post("/parked-cars", response_model=ParkedCar)
async def park_car(car: ParkedCarCreate, db: Session = Depends(get_db)):
    return create_parked_car(car, db)

@app.put("/parked-cars/{licensee}", response_model=ParkedCar)
async def leave_parking_lot(licensee: str, db: Session = Depends(get_db)):
    return update_car_leave(licensee, db)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)